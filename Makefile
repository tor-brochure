SOURCES = $(wildcard *.odg)
TARGETS = $(SOURCES:.odg=.pdf)

LIBREOFFICE_BIN = "/Applications/LibreOffice.app/Contents/MacOS/soffice"

%.pdf:%.odg
	$(LIBREOFFICE_BIN) --headless --convert-to pdf "$^"

all: $(TARGETS)

clean:
	rm -f $(TARGETS)

